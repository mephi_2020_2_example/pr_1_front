<!DOCTYPE html>
<head>
<title>Hello World portal</title>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<h1>Добро пожаловать на проектую практику</h1>
<?php
    $url = getenv('host') . "/api/v1/hello";
    $ch = curl_init($url);
    // print($url);
    curl_setopt($ch,CURLOPT_POST,false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // возвратить то что вернул сервер
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // следовать за редиректами
    $result = json_decode(curl_exec($ch));
    print($result->text);
    print('.<br> Местное время ');
    print($result->localTime);
?>
<script>
function get_rand_number() {
    $.ajax({
        url: "/rand.php",
        success: function(resp) {
            resp = JSON.parse(resp);
            $('#rand_number').text(resp.number);
        }
    });
}
</script>
<br>
<br>
<br>
<button onclick="get_rand_number()">Получить случайное число</button>
<br><br>
<div>Случайное число - <span id="rand_number">Вы еще не получили случайное число</span></div>
</body>